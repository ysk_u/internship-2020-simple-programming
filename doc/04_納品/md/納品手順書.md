# 納品手順書

1. ターミナルを開き、コマンド `npm run electron:build` を実行し、作成したアプリケーションをビルドします。
1. `dist_electron` フォルダに `job-info-manager-0.1.0-win.zip`というzipファイルが作成されるのを確認します。
1. 納品用のフォルダを作成し、以下のフォルダ構成で納品物を作成します。

    ```
    <各自の名前>
    ├doc
    │ ├設計書.pdf
    │ └テスト項目書.xlsx
    └app
    └job-info-manager-0.1.0-win.zip
    ```

1. [Google ドライブ](https://drive.google.com/drive/u/1/folders/1kU0VE0pfOqjE4JTKH6zcjtO4tO88lBMn)の `納品先` フォルダに、納品用フォルダを納品してください。
1. 以上で簡易システム開発は終了です。お疲れ様でした！！！