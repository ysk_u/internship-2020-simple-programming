import Vue from 'vue';
import App from './App.vue';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/ja';
import VueShortkey from 'vue-shortkey';

Vue.config.productionTip = false;

Vue.use(Element, { locale });
Vue.use(VueShortkey);

new Vue({
  render: h => h(App)
}).$mount('#app');
