/**
 * オブジェクトに関する汎用的な機能を提供するユーティリティクラス
 *
 * @export
 * @class ObjectUtil
 */
export class ObjectUtil {
  /**
   * 引数 `source` に設定されてた値が空かどうか判定し、結果を返却する
   *
   * 空オブジェクト ({}) は空の判定となる
   *
   * @static
   * @param {*} source
   * @returns
   * @memberof ObjectUtil
   */
  static isEmpty(source) {
    return source === null || source === undefined || Object.keys(source).length === 0;
  }

  /**
   * 引数 `source` に設定されてた値が空でないかどうか判定し、結果を返却する
   *
   * 空オブジェクト ({}) は空の判定となる
   *
   * @static
   * @param {*} source
   * @returns
   * @memberof ObjectUtil
   */
  static isNotEmpty(source) {
    return !this.isEmpty(source);
  }
}
