/**
 * 文字列に関する汎用的な機能を提供するユーティリティクラス
 *
 * @export
 * @class StringUtil
 */
export class StringUtil {
  /**
   * 引数 `source` に設定されてた値が空かどうか判定し、結果を返却する
   *
   * @static
   * @param {*} source
   * @returns sourceが空かどうか
   * @memberof StringUtil
   */
  static isEmpty(source) {
    return source === null || source === undefined || source === '';
  }

  /**
   * 引数 `source` に設定されてた値が空でないかどうか判定し、結果を返却する
   *
   * @static
   * @param {*} source
   * @returns sourceが空でないかどうか
   * @memberof StringUtil
   */
  static isNotEmpty(source) {
    return !this.isEmpty(source);
  }

  /**
   * 引数 `source` から、半角英数字以外の文字を除去して返却する (空白も除去)
   *
   * @static
   * @param {*} source
   * @return {*}
   * @memberof StringUtil
   */
  static deleteProhibitedCharacters(source) {
    const regExp = new RegExp(/[^0-9a-zA-Z]/g);
    return source.replace(regExp, '').trim();
  }
}
