import AppConfig from '../../app.config';

/**
 * 定数クラス
 *
 * (汎用的に利用する固定値はここで一元管理する)
 *
 * @export
 * @class Constants
 */
export class Constants {
  /** LocalStorageのキー */
  static LOCAL_STORAGE_KEY = 'jobInfoList';

  /** メッセージボックスに関する情報 */
  static MESSAGE_BOX = {
    TITLE: { CONFIRM: '確認', ERROR: 'エラー' },
    MESSAGE: {
      CONFIRM: {
        /** 削除確認メッセージ */
        DELETE: '選択したJob情報を削除します。よろしいですか？',
        /** アプリケーション終了確認メッセージ */
        QUIT: `${AppConfig.appTitle} を終了します。よろしいですか？`
      },
      ERROR: {
        /** Job情報が未選択なことによるエラー用 */
        NO_SELECTION: '対象のJob情報が選択されていません。',
        /** 必須項目が入力されていないことによるエラー用 */
        REQUIRED_ITEM_WAS_EMPTY: '必須項目がすべて入力されていません。',
        /** Job番号がDBと重複していた場合のエラー用 */
        JOB_NUMBER_IS_DUPLICATED: 'Job番号が重複しています。'
      }
    },
    BUTTON_LABEL: {
      CONFIRM: { OK: 'OK', CANCEL: 'キャンセル' },
      ERROR: { OK: 'OK' }
    }
  }

  /** ダイアログのモード情報 */
  static DIALOG_MODE = {
    /** 入力ダイアログ */
    INPUT: { ADD: 0, EDIT: 1 },
    /** 初期化用 */
    INITIAL: -1
  }

  /** エラーオブジェクト情報 */
  static ERROR = {
    /** Job番号がDBと重複していた場合のエラー */
    JOB_NUMBER_IS_DUPLICATED: {
      name: 'JOB_NUMBER_IS_DUPLICATED',
      message: 'Job番号が重複しています。'
    }
  }
}
