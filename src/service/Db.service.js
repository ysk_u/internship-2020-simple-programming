import { Constants } from '../constants/Constants';

/**
 * DB処理を行うクラス
 *
 * 今回はLocalStorageをDBと見立てて利用する
 *
 * ※個人情報などの見られてはまずいデータは、
 * LocalStorageに保存するのは大変危険！！！ (SessionStorageも同じく)
 *
 * @class DbService
 */
class DbService {
  /**
   * コンストラクタ (`new DbService();`を実行した際に呼び出される処理)
   *
   * DBとして扱うLocalStorageの初期化処理を行う
   *
   *
   * @memberof DbService
   */
  constructor() {
    const reservedData = localStorage.getItem(Constants.LOCAL_STORAGE_KEY);

    // Job情報一覧が存在しない場合は初期登録を行う
    if (!reservedData || !Array.isArray(JSON.parse(reservedData))) {
      localStorage.setItem(Constants.LOCAL_STORAGE_KEY, '[]');
    }
  }

  /**
   * DBからJob情報の一覧を取得して返却する
   *
   * @returns
   * @memberof DbService
   */
  getJobInfoList() {
    return JSON.parse(localStorage.getItem(Constants.LOCAL_STORAGE_KEY) || '[]');
  }

  /**
   * Job情報を追加する
   *
   * @param {*} info
   * @memberof DbService
   */
  addJobInfo(info) {
    const jobInfoList = JSON.parse(localStorage.getItem(Constants.LOCAL_STORAGE_KEY));

    if (jobInfoList && Array.isArray(jobInfoList) && jobInfoList.every((cur) => cur.jobNumber !== info.jobNumber)) {
      jobInfoList.push(info);
      localStorage.setItem(Constants.LOCAL_STORAGE_KEY, JSON.stringify(jobInfoList));
    } else {
      const error = new Error();
      error.name = Constants.ERROR.JOB_NUMBER_IS_DUPLICATED.name;
      error.message = Constants.ERROR.JOB_NUMBER_IS_DUPLICATED.message;
      throw error;
    }
  }

  /**
   * 指定したindexに設定されているJob情報をinfoで更新する
   *
   * @param {*} index
   * @param {*} info
   * @memberof DbService
   */
  updateJobInfo(info, oldInfo) {
    const oldInfoStr = JSON.stringify(oldInfo);
    const oldJobInfoList = JSON.parse(localStorage.getItem(Constants.LOCAL_STORAGE_KEY));

    const duplicateExists = oldJobInfoList.some((cur) => {
      return cur.jobNumber === info.jobNumber && cur.jobNumber !== oldInfo.jobNumber;
    });

    if (oldJobInfoList && Array.isArray(oldJobInfoList) && !duplicateExists) {
      let newJobInfoList = oldJobInfoList;
      newJobInfoList = oldJobInfoList.map((cur) => JSON.stringify(cur) === oldInfoStr ? info : cur);
      localStorage.setItem(Constants.LOCAL_STORAGE_KEY, JSON.stringify(newJobInfoList));
    } else {
      const error = new Error();
      error.name = Constants.ERROR.JOB_NUMBER_IS_DUPLICATED.name;
      error.message = Constants.ERROR.JOB_NUMBER_IS_DUPLICATED.message;
      throw error;
    }
  }

  /**
   * 指定したinfoと一致するJob情報を削除する
   *
   * @param {*} info
   * @memberof DbService
   */
  deleteJobInfo(info) {
    const infoStr = JSON.stringify(info);
    let jobInfoList = JSON.parse(localStorage.getItem(Constants.LOCAL_STORAGE_KEY));

    if (jobInfoList && Array.isArray(jobInfoList)) {
      jobInfoList = jobInfoList.filter((jobInfo) => JSON.stringify(jobInfo) !== infoStr);
      localStorage.setItem(Constants.LOCAL_STORAGE_KEY, JSON.stringify(jobInfoList));
    }
  }

  /**
   * テストデータを登録する
   *
   * @memberof DbService
   */
  registerSampleData() {
    const length = 100;
    const sampleData = Array.from({ length: length }, (_, index) => ({
      jobNumber: `job${`${index + 1}`.padStart(`${length}`.length, '0')}`,
      jobName: `sample job ${`${index + 1}`.padStart(`${length}`.length, '0')}`
    }));
    localStorage.setItem(Constants.LOCAL_STORAGE_KEY, JSON.stringify(sampleData));
  }

  /**
   * テストデータを全削除する
   *
   * @memberof DbService
   */
  deleteSampleData() {
    localStorage.setItem(Constants.LOCAL_STORAGE_KEY, '[]');
  }
}

export const dbService = new DbService();
