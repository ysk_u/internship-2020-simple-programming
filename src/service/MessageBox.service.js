import { Constants } from '../constants/Constants';

export class MessageBoxService {
  client = {};

  constructor(client) {
    this.client = client;
  }

  showConfirm(message) {
    return this.client.$confirm(message, Constants.MESSAGE_BOX.TITLE.CONFIRM, {
      confirmButtonText: Constants.MESSAGE_BOX.BUTTON_LABEL.CONFIRM.OK,
      cancelButtonText: Constants.MESSAGE_BOX.BUTTON_LABEL.CONFIRM.CANCEL,
      closeOnClickModal: false,
      closeOnPressEscape: false,
      type: 'warning'
    });
  }

  showError(message) {
    return this.client.$alert(message, Constants.MESSAGE_BOX.TITLE.ERROR, {
      confirmButtonText: Constants.MESSAGE_BOX.BUTTON_LABEL.ERROR.OK,
      closeOnClickModal: false,
      closeOnPressEscape: false,
      type: 'error'
    }).catch((e) => { /** 何もしない */ });
  }
}
