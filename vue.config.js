module.exports = {
  pluginOptions: {
    electronBuilder: {
      preload: './src/preload.js',
      builderOptions: {
        productName: 'job-info-manager',
        appId: 'com.sample.jobinfomanager',
        win: {
          target: [
            {
              target: 'zip', // 'zip', 'nsis', 'portable'
              arch: ['x64'] // 'x64', 'ia32'
            }
          ]
        }
      }
    }
  }
};
